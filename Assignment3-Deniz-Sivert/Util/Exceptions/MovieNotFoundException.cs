﻿namespace Assignment3_Deniz_Sivert.Util.Exceptions
{
    public class MovieNotFoundException : EntityNotFoundException
    {
        public MovieNotFoundException() : base("Exception - Movie Not Found!")
        {
        }
    }
}
