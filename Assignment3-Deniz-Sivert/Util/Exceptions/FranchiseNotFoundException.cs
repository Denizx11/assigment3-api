﻿namespace Assignment3_Deniz_Sivert.Util.Exceptions
{
    public class FranchiseNotFoundException : EntityNotFoundException
    {
        public FranchiseNotFoundException() : base("Exception - Franchise Not Found!")
        {
        }
    }
}
