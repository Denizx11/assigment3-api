﻿namespace Assignment3_Deniz_Sivert.Util.Exceptions
{
    public class CharacterNotFoundException : EntityNotFoundException
    {
        public CharacterNotFoundException() : base("Exception - Character Not Found!")
        {
        }
    }
}
