﻿using Assignment3_Deniz_Sivert.Models;
using Assignment3_Deniz_Sivert.Util.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Assignment3_Deniz_Sivert.Services.Character
{
    public class CharacterService : ICharacterServices
    {
        private readonly MovieDBContext _movieDBContext;
        private readonly ILogger<CharacterService>? _logger;
        public CharacterService(MovieDBContext movieDBContext, ILogger<CharacterService> logger)
        {
            _movieDBContext = movieDBContext;
            _logger = logger;
        }

        public async Task AddAsync(Characters entity)
        {
            await _movieDBContext.AddAsync(entity);
            await _movieDBContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            //Finds an entity with the given primarykey
            var character = await _movieDBContext.Characters.FindAsync(id);

            //Log  and throw pattern
            if (character == null)
            {
                _logger.LogError("Character not found with id: " + id);
                throw new CharacterNotFoundException();
            }

            // Set to nullable relationships
            // so it removes the FKs when delete it.
            _movieDBContext.Characters.Remove(character);
            await _movieDBContext.SaveChangesAsync();
        }

        public async Task<ICollection<Characters>> GetAllAsync()
        {
            return await _movieDBContext.Characters
                 .Include(p => p.Movies)
                 .ToListAsync();
        }

        public async Task<Characters> GetByIdAsync(int id)
        {
            // Log and throw error
            if (!await CharacterExistsAsync(id))
            {
                _logger.LogError("Character not found with Id: " + id);
                throw new CharacterNotFoundException();
            }

            // Include all related data for Character
            return await _movieDBContext.Characters
                .Where(p => p.Id == id)
                .Include(p => p.Movies)
                .FirstAsync();
        }

        public async Task UpdateAsync(Characters entity)
        {
            // Log and throw Error
            if (!await CharacterExistsAsync(entity.Id))
            {
                _logger.LogError("Character not found with id: " + entity.Id);
                throw new CharacterNotFoundException();
            }
            _movieDBContext.Entry(entity).State = EntityState.Modified;
            await _movieDBContext.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a Character exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If Character exists</returns>
        /// <exception cref="CharacterNotFoundException">Characters not found!</exception>
        private async Task<bool> CharacterExistsAsync(int id)
        {
            return await _movieDBContext.Movies.AnyAsync(e => e.Id == id);
        }
    }
}
