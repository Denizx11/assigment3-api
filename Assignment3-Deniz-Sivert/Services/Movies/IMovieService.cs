﻿using Assignment3_Deniz_Sivert.Models;
using Assignment3_Deniz_Sivert.Util.Exceptions;

namespace Assignment3_Deniz_Sivert.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        /// <summary>
        /// Gets all Characters in a movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <exception cref="CharacterNotFoundException">Thrown when Characters does not exist</exception>
        Task <ICollection<Characters>> GetCharactersAsync(int movieId);

        /// <summary>
        /// Gets all Characters in a mocie
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="movieId"></param>
        /// <exception cref="MovieNotFoundException">Thrown when MovieID does not exist</exception>
        Task UpdateCharactersAsync(int[] characterId, int movieId);
    }
}
