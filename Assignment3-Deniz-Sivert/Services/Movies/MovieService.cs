﻿using Assignment3_Deniz_Sivert.Models;
using Assignment3_Deniz_Sivert.Util.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Assignment3_Deniz_Sivert.Services.Movies
{
    public class MovieService : IMovieService
    {
        private readonly MovieDBContext _movieDbContext;
        private readonly ILogger<MovieService> _logger;

        public MovieService(MovieDBContext movieDbContext, ILogger<MovieService> logger)
        {
            _movieDbContext = movieDbContext;
            _logger = logger;
        }
        public async Task AddAsync(Movie entity)
        {
            await _movieDbContext.AddAsync(entity);
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            //Finds an entity with the given primarykey
            var movie = await _movieDbContext.Movies.FindAsync(id);

            //Log  and throw pattern
            if (movie == null)
            {
                _logger.LogError("Movie not found with id: " + id);
                throw new MovieNotFoundException();
            }

            // Set to nullable relationships
            // so it removes the FKs when delete it.

            _movieDbContext.Movies.Remove(movie);
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _movieDbContext.Movies
                 .Include(p => p.Characters)
                 .ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            // Log and throw error
            if (!await MovieExistsAsync(id))
            {
                _logger.LogError("Movie not found with Id: " + id);
                throw new MovieNotFoundException();
            }

            // Include all related data for Movie
            return await _movieDbContext.Movies
                .Where(p => p.Id == id)
                .Include(p => p.Characters)
                .FirstAsync();
        }

        public async Task<ICollection<Characters>> GetCharactersAsync(int movieId)
        {
            // Log and throw Error
            if (!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Movie not found with Id: " + movieId);
            }
            // Don't need to include related data because of the DTO we are mapping to.
            // This can change depending on the business requirements.
            return await _movieDbContext.Characters
                .Where(s => s.Id == movieId)
                .ToListAsync();
        }

        public async Task UpdateAsync(Movie entity)
        {
            // Log and throw Error
            if (!await MovieExistsAsync(entity.Id))
            {
                _logger.LogError("Movie not found with id: " + entity.Id);
                throw new MovieNotFoundException();
            }
            _movieDbContext.Entry(entity).State = EntityState.Modified;
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task UpdateCharactersAsync(int[] characterId, int movieId)
        {
            // Log and throw pattern
            if (!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Professor not found with Id: " + movieId);
                throw new MovieNotFoundException();
            }
            // First convert the int[] to List<Character>

            // Could utilize the MovieNotFoundException here
            // if there is an Id in the array that doesnt have a character.
            // That would require a different interaction with EF to check for exception.
            // It is not done in this implementation.
            List<Characters> character = characterId
                .ToList()
                .Select(sid => _movieDbContext.Characters
                .Where(s => s.Id == sid).First())
                .ToList();
            // Get Movie for Id
            Movie movie = await _movieDbContext.Movies
                .Where(p => p.Id == movieId)
                .FirstAsync();
            // Set the movie characters
            movie.Characters = character;
            _movieDbContext.Entry(movie).State = EntityState.Modified;
            // Save all the changes
            await _movieDbContext.SaveChangesAsync();

        }

        /// <summary>
        /// Simple utility method to help check if a Movie exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If Movie exists</returns>
        /// <exception cref="MovieNotFoundException"
        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _movieDbContext.Movies.AnyAsync(e => e.Id == id);
        }
    }
}
