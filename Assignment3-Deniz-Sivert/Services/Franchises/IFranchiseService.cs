﻿using Assignment3_Deniz_Sivert.Models;
using Assignment3_Deniz_Sivert.Util.Exceptions;

namespace Assignment3_Deniz_Sivert.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        /// <summary>
        /// Gets all Movies in a Franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <exception cref="FranchiseNotFoundException">Thrown when Franchise does not exist</exception>
        Task<ICollection<Movie>> GetMoviesAsync(int franchiseId);
        /// <summary>
        /// Update Movies in Franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movieIds"></param>
        /// <exception cref="FranchiseNotFoundException">Thrown when Franchise does not exist</exception>
        Task UpdateMoviesAsync(int[] movieIds,int franchiseId);
    }
}
