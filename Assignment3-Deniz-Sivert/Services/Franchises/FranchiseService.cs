﻿using Assignment3_Deniz_Sivert.Models;
using Assignment3_Deniz_Sivert.Util.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Assignment3_Deniz_Sivert.Services.Franchises
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDBContext _movieDBContext;
        private readonly ILogger<FranchiseService> _logger;
        public FranchiseService(MovieDBContext movieDBContext, ILogger<FranchiseService> logger)
        {
            _movieDBContext = movieDBContext;
            _logger = logger;
        }

        public async Task AddAsync(Franchise entity)
        {
            await _movieDBContext.AddAsync(entity);
            await _movieDBContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            //Finds an entity with the given primarykey
            var franchise = await _movieDBContext.Franchises.FindAsync(id);

            //Log  and throw pattern
            if (franchise == null)
            {
                _logger.LogError("Franchise not found with id: " + id);
                throw new FranchiseNotFoundException();
            }

            // Set to nullable relationships
            // so it removes the FKs when delete it.

            _movieDBContext.Franchises.Remove(franchise);
            await _movieDBContext.SaveChangesAsync();
        }

        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _movieDBContext.Franchises
                .Include(p => p.Movies)
                .ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            // Log and throw error
            if (!await FranchiseExistsAsync(id))
            {
                _logger.LogError("Franchise not found with Id: " + id);
                throw new FranchiseNotFoundException();
            }

            // Include all related data for Movie
            return await _movieDBContext.Franchises
                .Where(p => p.Id == id)
                .Include(p => p.Movies)
                .FirstAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesAsync(int franchiseId)
        {
            // Log and throw Error
            if (!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Movie not found with Id: " + franchiseId);
                throw new FranchiseNotFoundException();
            }
            // Don't need to include related data because of the DTO we are mapping to.
            // This can change depending on the business requirements.
            return await _movieDBContext.Movies
                .Where(s => s.FranchiseId == franchiseId)
                .ToListAsync();
        }

        public async Task UpdateAsync(Franchise entity)
        {
            // Log and throw Error
            if (!await FranchiseExistsAsync(entity.Id))
            {
                _logger.LogError("Franchise not found with id: " + entity.Id);
                throw new MovieNotFoundException();
            }
            _movieDBContext.Entry(entity).State = EntityState.Modified;
            await _movieDBContext.SaveChangesAsync();
        }

        public async Task UpdateMoviesAsync(int[] movieIds, int franchiseId)
        {
            // Log and throw pattern
            if (!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Professor not found with Id: " + franchiseId);
                throw new MovieNotFoundException();
            }
            // First convert the int[] to List<Character>

            // Could utilize the MovieNotFoundException here
            // if there is an Id in the array that doesnt have a character.
            // That would require a different interaction with EF to check for exception.
            // It is not done in this implementation.
            List<Movie> movies = movieIds
                .ToList()
                .Select(mid => _movieDBContext.Movies
                .Where(s => s.Id == mid).First())
                .ToList();
            // Get Movie for Id
            Franchise franchise = await _movieDBContext.Franchises
                .Where(p => p.Id == franchiseId)
                .FirstAsync();
            // Set the movie characters
            franchise.Movies = movies;
            _movieDBContext.Entry(movies).State = EntityState.Modified;
            // Save all the changes
            await _movieDBContext.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a Franchise exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If Movie exists</returns>
        /// <exception cref="FranchiseNotFoundException">Franchise not found</exception>
        private async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _movieDBContext.Franchises.AnyAsync(e => e.Id == id);
        }
    }
}
