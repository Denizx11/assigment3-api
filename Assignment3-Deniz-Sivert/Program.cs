using Assignment3_Deniz_Sivert.Models;
using Assignment3_Deniz_Sivert.Services.Character;
using Assignment3_Deniz_Sivert.Services.Franchises;
using Assignment3_Deniz_Sivert.Services.Movies;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Configuring generated XML docs for Swagger
var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Postgrad API",
        Description = "Simple API to manage postgraduate studies",
        Contact = new OpenApiContact
        {
            Name = "Nicholas Lennox",
            Url = new Uri("https://gitlab.com/NicholasLennox")
        },
        License = new OpenApiLicense
        {
            Name = "MIT 2022",
            Url = new Uri("https://opensource.org/licenses/MIT")
        }
    });
    //options.IncludeXmlComments(xmlPath);

});
builder.Services.AddDbContext<MovieDBContext> (
    opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("MovieDB")));

// Automapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
// Adding logging through ILogger
builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
});
// Custom Services
builder.Services.AddTransient<ICharacterServices, CharacterService>(); // Transient is the default behaviour and means a new instance is made when injected.
builder.Services.AddTransient<IMovieService, MovieService>();
builder.Services.AddTransient<IFranchiseService, FranchiseService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
