﻿using Microsoft.EntityFrameworkCore;

namespace Assignment3_Deniz_Sivert.Models
{
    public class MovieDBContext : DbContext
    {
        public MovieDBContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Characters> Characters { get; set; }
        public DbSet<Franchise> Franchises{ get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 1,
                MovieTitle = "Nemo",
                Genre = "Animation",
                Director = "Andrew Stanton",
                ReleaseYear = 2003,
                Picture= "https://lumiere-a.akamaihd.net/v1/images/p_findingnemo_19752_05271d3f.jpeg?region=0%2C0%2C540%2C810",
                Trailer = "https://www.youtube.com/watch?v=2zLkasScy7A",
                FranchiseId = 1
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                MovieTitle = "Toy Story",
                Genre = "Animation",
                Director = "John Lasseter",
                ReleaseYear = 1995,
                Picture = "https://m.media-amazon.com/images/M/MV5BMDU2ZWJlMjktMTRhMy00ZTA5LWEzNDgtYmNmZTEwZTViZWJkXkEyXkFqcGdeQXVyNDQ2OTk4MzI@._V1_.jpg",
                Trailer = "https://www.youtube.com/watch?v=rNk1Wi8SvNc",
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                MovieTitle = "Ip Man",
                Genre = "Action",
                Director = "Wilson Yip",
                ReleaseYear = 2008,
                Picture = "https://m.media-amazon.com/images/M/MV5BNTFmMjM3M2UtOTIyZC00Zjk3LTkzODUtYTdhNGRmNzFhYzcyXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_FMjpg_UX1000_.jpg",
                Trailer = "https://www.youtube.com/watch?v=RBYbqO_FUA4",
                
            });


            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "Disney",
                Description = "The Walt Disney Company, commonly known as Disney is an American multinational mass media and entertainment" +
                " conglomerate headquartered at the Walt Disney Studios complex in Burbank, California"

            });

            

            modelBuilder.Entity<Characters>().HasData(new Characters
            {
                Id = 1,
                FullName = "Dory",
                Alias = "Little Blue",
                Gender = "Female",
                Picture = "https://i0.wp.com/p3.no/filmpolitiet/wp-content/uploads/2016/08/Oppdrag-Dory-bilde-1.jpg"

            });
            modelBuilder.Entity<Characters>().HasData(new Characters
            {
                Id = 2,
                FullName = "Buzz Lightyear",
                Gender = "Male",
                Picture = "https://en.wikipedia.org/wiki/Buzz_Lightyear#/media/File:Buzz_Lightyear.png"

            });
            modelBuilder.Entity<Characters>().HasData(new Characters
            {
                Id = 3,
                FullName = "Ip Man",
                Alias = "Master Ip",
                Gender = "Male",
                Picture = "https://static.wikia.nocookie.net/p__/images/f/f6/Ip_Man_%28Donnie_Yen%29.png/revision/latest?cb=20201201065502&path-prefix=protagonist"

            });


            modelBuilder.Entity<Movie>()
                .HasMany(c => c.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                "MoveCharacters",
                r => r.HasOne<Characters>().WithMany().HasForeignKey("CharacterId"),
                l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                je =>
                {
                    je.HasKey("CharacterId", "MovieId");
                    je.HasData(
                        new { CharacterId = 1, MovieId = 1 },
                        new { CharacterId = 2, MovieId = 2 },
                        new { CharacterId = 3, MovieId = 3 }
                        );
                }
                );
        }
    }
}
